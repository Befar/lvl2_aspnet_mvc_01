﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LVL2_ASPNet_MVC_01.Controllers
{

    public class CodingIDController : Controller
    {
        // GET: CodingID
        [Route("CodingID/Display")]
        [HttpGet]
       
        public ActionResult CodingIDMassage()
        {
            string html = "<form method='post' >" +
                       "<input type='text' name = 'name' / >" +
                       "<input type='submit' value='Great Me'/>" +
                       "</form>";

            return Content(html, "text/html");
        }
        [Route("CodingID/Display")]
        [HttpPost]

        public ActionResult Display(string name = "World")
        {


            return Content(string.Format("<h1>Hello " + name + "</h1>"), "text/html");
        }

        [Route("CodingID/Aloha")]
        public ActionResult Goodbye()
        {
            return Content("Goodbye");
        }
     
 

    }
}